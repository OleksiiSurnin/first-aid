//@ts-check
import Item from './Item';

const Reactive = require('Reactive');
const Textures = require('Textures');
const Scene = require('Scene');
const NativeUI = require('NativeUI');
const TouchGestures = require('TouchGestures');
const Diagnostics = require('Diagnostics');

export default class Inventory {
    constructor() {
        /** @type {Item[]} */
        this.items = [];
        
        this.pickerIndex = 0;
        NativeUI.picker.selectedIndex.monitor().subscribe(index => {
            this.pickerIndex = index.newValue;
        });

        

    }

    linkPicker(picker) {
        this.picker = picker;
    }

    /**
     *
     *
     * @param {Item[]} items
     * @memberof Inventory
     */
    registerItems(...items) {
        items.forEach(item => {
            TouchGestures.onTap(item.pickupCollider).subscribe(() => {
                this.addItem(item);
            });
        });
    }

    /**
     *
     *
     * @param {Item[]} items
     * @returns
     * @memberof Inventory
     */
    addItem(...items) {
        // return if inventory already has this item
        if (this.items.some(item => items.forEach(suggestedItem => item === suggestedItem))) return;
        else {
            this.items.push(...items);
            items.forEach(item => item.sceneObject.hidden = Reactive.val(true));
            this._redrawPicker();
        }
        Diagnostics.log('Item added');
    }

    removeItem(...items) {
        // return if inventory does not have this item
        if (this.items.some(item => items.forEach(suggestedItem => item === suggestedItem))) return;
        else {
            items.forEach(item => {
                this.items.splice(this.items.indexOf(item), 1);
            });
            this._redrawPicker();
        }
    }

    /**
     * Redraw inventory UI picker with current items
     *
     * @memberof Inventory
     */
    _redrawPicker() {
        // Do not draw empty picker, because it breaks Instagram camera
        if (this.items.length === 0) {
            NativeUI.picker.visible = Reactive.val(false);
            return;
        }

        const pickerItems = [];
        this.items.forEach(item => pickerItems.push({ image_texture: item.texture }));
        const configuration = { 
            selectedIndex: this.pickerIndex, // select recently picked up item
            items: pickerItems
        };
    
        NativeUI.picker.configure(configuration);
        NativeUI.picker.visible = Reactive.val(true);
    }
}
