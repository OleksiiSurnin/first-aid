//#region Spark AR modules
const Scene = require('Scene');
const Camera = Scene.root.find('Camera');
const CameraInfo = require('CameraInfo');
const Patches = require('Patches');
const Materials = require('Materials');
const Textures = require('Textures');
const Reactive = require('Reactive');
const Audio = require('Audio');
const NativeUI = require('NativeUI');
const FaceTracking = require('FaceTracking');
const Diagnostics = require('Diagnostics');
const TouchGestures = require('TouchGestures');
const Animation = require('Animation');
const Time = require('Time');
//#endregion

import './Polyfills';
import ObjectManager from './ObjectManager';
import Inventory from './Inventory';
import Item from './Item';
import Reanimation from './Reanimation';

ObjectManager.Builder
    //#region Bleeding minigame
    .load('belt', 'planeTracker0/scenario_bleeding/items/belt')
    .load('belt_pickup', 'planeTracker0/scenario_bleeding/items/belt/pickup_collider')
    .load('paper', 'planeTracker0/scenario_bleeding/items/paper')
    .load('paper_pickup', 'planeTracker0/scenario_bleeding/items/paper/pickup_collider')
    //#endregion
    //#region CPR minigame
    .load('CPR_game', 'planeTracker0/scenario_CPR/UI/CPR_game')
    .load('CPR_inner_circle', 'planeTracker0/scenario_CPR/UI/CPR_game/circles/inner_circle')
    .load('CPR_outter_circle', 'planeTracker0/scenario_CPR/UI/CPR_game/circles/outter_circle')
    .load('compression_counter', 'planeTracker0/scenario_CPR/UI/CPR_game/text_count/count')

    .load('RB_game', 'planeTracker0/scenario_CPR/UI/RB_game')
    .load('RB_inner_circle', 'planeTracker0/scenario_CPR/UI/RB_game/circles/inner_circle')
    .load('RB_outter_circle', 'planeTracker0/scenario_CPR/UI/RB_game/circles/outter_circle')
    .load('breath_counter', 'planeTracker0/scenario_CPR/UI/RB_game/text_count/count')

    .load('head', 'planeTracker0/scenario_CPR/patient/Head')
    .load('hands', 'planeTracker0/scenario_CPR/patient/Hands')
    .load('cellphone', 'planeTracker0/scenario_CPR/items/cellphone')
    .load('cellphone_call', 'Camera/Focal Distance/overlay_items/cellphone')
    .load('man', 'planeTracker0/scenario_CPR/patient/Man')
    .load('CPR_rate', 'Camera/canvas0/CPR_rate')

    .load('finger_tap_phone', 'planeTracker0/scenario_CPR/items/cellphone/finger_tap_phone')
    .load('finger_pinch_chest', 'planeTracker0/scenario_CPR/items/finger_pinch_chest')
    .load('finger_pan_head_back', 'planeTracker0/scenario_CPR/items/finger_pan_head_back')
    .load('finger_pan_head_straight', 'planeTracker0/scenario_CPR/items/finger_pan_head_straight')
    //#endregion
.ready.then(() => {try {
    //#region CPR minigame configuration
    let reanimation = new Reanimation({
        CPR_game: ObjectManager.getObject('CPR_game'),
        CPR_inner_circle: ObjectManager.getObject('CPR_inner_circle'),
        CPR_outter_circle: ObjectManager.getObject('CPR_outter_circle'),
        compression_counter: ObjectManager.getObject('compression_counter'),

        RB_game: ObjectManager.getObject('RB_game'),
        RB_inner_circle: ObjectManager.getObject('RB_inner_circle'),
        RB_outter_circle: ObjectManager.getObject('RB_outter_circle'),
        breath_counter: ObjectManager.getObject('breath_counter'),

        head: ObjectManager.getObject('head'),
        hands: ObjectManager.getObject('hands'),
        cellphone: ObjectManager.getObject('cellphone'),
        cellphone_call: ObjectManager.getObject('cellphone_call'),
        CPR_rate: ObjectManager.getObject('CPR_rate'),

        finger_tap_phone: ObjectManager.getObject('finger_tap_phone'),
        finger_pinch_chest: ObjectManager.getObject('finger_pinch_chest'),
        finger_pan_head_back: ObjectManager.getObject('finger_pan_head_back'),
        finger_pan_head_straight: ObjectManager.getObject('finger_pan_head_straight'),
        
        man: ObjectManager.getObject('man')
    });
    //#endregion

    reanimation.start();

    Diagnostics.log('Loaded without errors');
    Scene.root.find('text').text = Scene.root.find('text').text.pinLastValue() + '\nGame loaded';
} catch (error) {
    Diagnostics.log(error.message);
}
});
