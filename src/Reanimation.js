import Animations from './Animations';

const Audio = require('Audio');
const Scene = require('Scene');
const Reactive = require('Reactive');
const Patches = require('Patches');
const TouchGestures = require('TouchGestures');
const Diagnostics = require('Diagnostics');
const Time = require('Time');

// CPR minigame
export default class Reanimation {
    /**
     * Creates an instance of Reanimation.
     * @param {Object} objects
     * @param {SceneObjectBase} objects.CPR_game
     * @param {SceneObjectBase} objects.CPR_inner_circle
     * @param {SceneObjectBase} objects.CPR_outter_circle
     * @param {SceneObjectBase} objects.compression_counter
     * 
     * @param {SceneObjectBase} objects.RB_game
     * @param {SceneObjectBase} objects.RB_inner_circle
     * @param {SceneObjectBase} objects.RB_outter_circle
     * @param {SceneObjectBase} objects.breath_counter
     * 
     * @param {SceneObjectBase} objects.CPR_rate
     * @param {SceneObjectBase} objects.hands
     * @param {SceneObjectBase} objects.head
     * @param {SceneObjectBase} objects.cellphone
     * @param {SceneObjectBase} objects.cellphone_call
     * @param {SceneObjectBase} objects.man
     * 
     * @param {SceneObjectBase} objects.finger_tap_phone
     * @param {SceneObjectBase} objects.finger_pinch_chest
     * @param {SceneObjectBase} objects.finger_pan_head_back
     * @param {SceneObjectBase} objects.finger_pan_head_straight
     * @memberof Reanimation
     */
    constructor(objects) {
        this.objects = objects;

        this.cycles = 0;
    }

    start() {
        // Initialization
        this.objects.CPR_inner_circle.hidden = Reactive.val(true);
        this.objects.CPR_outter_circle.hidden = Reactive.val(true);
        this.objects.cellphone.hidden = Reactive.val(true);
        this.objects.compression_counter.hidden = Reactive.val(true);

        this.objects.RB_game.hidden = Reactive.val(true);

        // Robot speech about trying to shout at injured and call 911
        Time.setTimeout(() => Audio.getPlaybackController('01-hello').setPlaying(true), 1000);
        Time.setTimeout(() => Audio.getPlaybackController('02-hail-injured').setPlaying(true), 5000);
        Time.setTimeout(() => Audio.getPlaybackController('03-call-911').setPlaying(true), 11000);
        Time.setTimeout(() => Audio.getPlaybackController('04-call-text').setPlaying(true), 15000);
        Time.setTimeout(() => {
            this.call911();
        }, 30);
    }

    call911() {
        // 911 call
        this.objects.cellphone.hidden = Reactive.val(false);

        Time.setTimeout(() => {
            this.objects.finger_tap_phone.hidden = Reactive.val(false);

            let sub_cellphone_pickup = TouchGestures.onTap(this.objects.cellphone).subscribe(() => {
                sub_cellphone_pickup.unsubscribe();
                this.objects.finger_tap_phone.hidden = Reactive.val(true);
                // cellphone animation and sound
                // ...
                this.objects.cellphone.hidden = Reactive.val(true);
                this.objects.cellphone_call.hidden = Reactive.val(false);

                //Diagnostics.log('Player: There is a male, age 35, unconscious, not moving and breathing. I am on Tiraspol street 41, begin CPR');
                Time.setTimeout(() => Audio.getPlaybackController('05-call-player').setPlaying(true), 500);
                Time.setTimeout(() => this.objects.cellphone_call.hidden = Reactive.val(true), 9500);
                Time.setTimeout(() => Audio.getPlaybackController('06-tear-clothes').setPlaying(true), 10000);
                            
                // after call
                Time.setTimeout(() => {
                    this.objects.hands.hidden = Reactive.val(false);
                    Animations.play(Animations.clips.REANIMATION_BEGIN);
                    this.objects.finger_pinch_chest.hidden = Reactive.val(false)
                    // TODO: replace with TouchGestures.onPinch()
                    let sub_shirt_tear = TouchGestures.onPinch(this.objects.man).subscribe(() => {
                        sub_shirt_tear.unsubscribe();
                        this.objects.finger_pinch_chest.hidden = Reactive.val(true)
                        Animations.play(Animations.clips.SHIRT_TEARING);
                        Time.setTimeout(() => this.beginCPR(), 1000);
                    });
                }, 10000) // should be another
            });            
        }, 15000)
    }

    beginCPR() {
        this.objects.CPR_inner_circle.hidden = Reactive.val(false);
        this.objects.CPR_outter_circle.hidden = Reactive.val(false);
        this.objects.compression_counter.hidden = Reactive.val(false);
        this.objects.compression_counter.text = 'x0';

        // hide RB
        this.objects.RB_game.hidden = Reactive.val(true);

        Patches.setBooleanValue('CPR_rate_overlay', Reactive.val(true));

        let beginDate = Date.now();
        let compressions = 0;

        let lastDate = beginDate; // last
        let penultimateDate = lastDate; // last but one

        let sub_to_tap;

        Audio.getPlaybackController('07-push').setPlaying(true);

        // CPR begins after first compression
        let doCompression = () => {
            let newDate = Date.now();
            let delta = (newDate - lastDate) / 1000;
            
            let rate = (1 / delta).toFixed(2);
            // @ts-ignore
            this.objects.CPR_rate.text = 'CPR rate: ' + rate + ' c/s (+)';
            Patches.setScalarValue('CPR_rate', Reactive.val(Number(rate)));

            penultimateDate = lastDate;
            lastDate = newDate;
            compressions++;

            Animations.play(Animations.clips.CPR_PUSH);

            Patches.setPulseValue('CPR_counter_updated', Reactive.once());
            this.objects.compression_counter.text = 'x' + compressions;

            if (compressions >= 15) {
                sub_to_tap.unsubscribe();
                Time.setTimeout(() => this.beginRB(), 400);
            }
        }

        let sub_first_tap = TouchGestures.onTap(this.objects.man).subscribe(() => {
            // Compression when chest is tapped
            sub_to_tap = TouchGestures.onTap(this.objects.man).subscribe(() => {
                doCompression();
            });
            // Update compressions rate and redraw circles
            Time.setInterval(() => {
                let newDate = Date.now();
                let delta = (newDate - penultimateDate) / 1000;
                // test
                let rate = (1 / delta).toFixed(2);
                // @ts-ignore
                this.objects.CPR_rate.text = 'CPR rate: ' + rate + ' c/s (-)';
                Patches.setScalarValue('CPR_rate', Reactive.val(Number(rate)));
            }, 500);
            doCompression();
            sub_first_tap.unsubscribe();
        });
    }

    // rescue breathing
    beginRB() {
        // hide CPR
        this.objects.CPR_inner_circle.hidden = Reactive.val(true);
        this.objects.CPR_outter_circle.hidden = Reactive.val(true);
        this.objects.compression_counter.hidden = Reactive.val(true);
        Patches.setBooleanValue('CPR_rate_overlay', Reactive.val(false));

        Audio.getPlaybackController('08-now-deliver-rb').setPlaying(true);
        this.objects.finger_pan_head_back.hidden = Reactive.val(false);

        // ask to tilt head back
        Animations.play(Animations.clips.HEAD_RB_START);
        // TODO: replace to onPan
        let sub_to_tilt = TouchGestures.onPan(this.objects.man).subscribe(() => {
            sub_to_tilt.unsubscribe();

            Audio.getPlaybackController('09-place-head').setPlaying(true);
            this.objects.finger_pan_head_back.hidden = Reactive.val(true);

            // show RB
            this.objects.RB_game.hidden = Reactive.val(false);
            this.objects.RB_inner_circle.hidden = Reactive.val(false);
            this.objects.RB_outter_circle.hidden = Reactive.val(false);
            this.objects.breath_counter.hidden = Reactive.val(false);
            this.objects.breath_counter.text = 'x0';

            Animations.play(Animations.clips.HEAD_RB_TILT_BACK);
            this.objects.head.hidden = Reactive.val(false);
            
            let breaths = 0;
            let sub_to_tap = TouchGestures.onTap(this.objects.man).subscribe(() => {
                // sound
                breaths++;
                this.objects.breath_counter.text = 'x' + breaths;
                Patches.setPulseValue('RB_counter_updated', Reactive.once());

                if (breaths >= 2) {
                    sub_to_tap.unsubscribe();

                    this.objects.finger_pan_head_straight.hidden = Reactive.val(false);

                    // TODO: replace to pan
                    let sub_to_tilt_straight = TouchGestures.onPan(this.objects.man).subscribe(() => {
                        sub_to_tilt_straight.unsubscribe();

                        this.objects.finger_pan_head_straight.hidden = Reactive.val(true);

                        Animations.play(Animations.clips.HEAD_RB_TILT_STRAIGHT);

                        this.cycles++;
                        if (this.cycles === 2) {
                            Audio.getPlaybackController('11-well-done').setPlaying(true);
                        } else {
                            Time.setTimeout(() => Animations.play(Animations.clips.HEAD_RB_STOP), 800);
                            Time.setTimeout(() => this.beginCPR(), 1600);
                            Audio.getPlaybackController('10-continue-cpr').setPlaying(true);
                        }

                        Time.setTimeout(() => this.objects.head.hidden = Reactive.val(true), 800);
                    })
                }
            });            
        });

        Diagnostics.log('Chapie: Rescue breathing!');
    }
}